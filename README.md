 CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Countdown module adds a block titled "Countdown" to count the days, hours,
minutes, and seconds since or until a specified event relative to the server's
clock.

 * For a full description of the module visit:
   https://www.drupal.org/project/countdown

 * To submit bug reports and feature suggestions, or to track changes
   visit: https://www.drupal.org/project/issues/countdown


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the Countdown module as you would normally install a contributed Drupal
module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the Countdown module.
    2. Navigate to Administration > Structure > Block and select which region
       and select "Place block" again.
    3. Enter the Title, Event Name, Event URL, and select the desired accuracy
       in days, hours, minutes, or seconds.
    4. Select the Target Date/Time.
    5. Select the content type to display the block on.
    6. There is also the functionality to restrict which pages the block is
       visible on and which roles can see it. The region can also be changed.
       Save block.


MAINTAINERS
-----------

 * Rahul Nahar (rahul.nahar001) - https://www.drupal.org/u/rahulnahar001
 * Joachim Noreiko (joachim) - https://www.drupal.org/u/joachim
 * deekayen - https://www.drupal.org/u/deekayen
 * Mahyar SBT (mahyarsbt) - https://www.drupal.org/u/mahyarsbt
